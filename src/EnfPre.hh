/*
 *  EnfPre.hh
 *
 *  created: Jan 2017
 *   author: Matthias Rungger
 */

/** @file **/

#ifndef ENFPRE_HH_
#define ENFPRE_HH_

#include <iostream>
#include <memory>

#include "SymbolicSet.hh"
#include "SymbolicModel.hh"


namespace scots {

/**
 * @class EnfPre
 * 
 * @brief computes of the enforcable predecessor 
 * 
 * Let \f$ F:X\times U \rightrightarrows X\f$ be the transition function and \f$
 * Z\subseteq X\f$, then \n\n
 *
 *  \f$\mathrm{pre}(Z)=\{ (x,u) \in X\times U \mid F(x,u)\neq \emptyset \wedge  F(x,u)\subseteq Z\}\f$
 *
 **/
class EnfPre {
protected:
  /* stores the permutation array used to swap pre with post variables */
  std::unique_ptr<int[]> m_permute;
  /* transition relation */
  BDD m_tr;  
  /* transition relation with m_cube_post abstracted */
  BDD m_tr_nopost;  
  /* BDD cubes with input and post variables */
  BDD m_cube_post;
  BDD m_cube_input;

public:
  /** @brief initialize the enforcable predecessor
   *  
   * @param manager - the Cudd manager
   * @param transition_relation - the BDD encoding the transition function of the SymbolicModel\n 
   *                              computed with SymbolicModel::compute_gb
   * @param  model - SymbolicModel containing the SymbolicSet for the state and input alphabet 
   **/
  template<class state_type, class input_type>
  EnfPre(const Cudd& manager, 
         const BDD& transition_relation,
         const SymbolicModel<state_type,input_type>& model) : m_tr(transition_relation) {
    /* the permutation array */
    size_t size = manager.ReadSize();
    m_permute = std::unique_ptr<int[]>(new int[size]);
    std::iota(m_permute.get(),m_permute.get()+size,0);
    auto pre_ids = model.get_sym_set_pre().get_bdd_var_ids();
    auto post_ids = model.get_sym_set_post().get_bdd_var_ids();
    for(size_t i=0; i<pre_ids.size(); i++)
      m_permute[pre_ids[i]]=post_ids[i];
    /* create a cube with the input bdd vars */
    m_cube_input = model.get_sym_set_input().get_cube(manager);
    /* create a cube with the post bdd vars */
    m_cube_post = model.get_sym_set_post().get_cube(manager);
    /* copy the transition relation */
    m_tr_nopost=m_tr.ExistAbstract(m_cube_post);
  }
  /** @brief computes the enforcable predecessor of the BDD Z **/
  BDD operator()(BDD Z) const {
    /* project onto state alphabet */
    Z=Z.ExistAbstract(m_cube_post*m_cube_input);
    /* swap variables */
    Z=Z.Permute(m_permute.get());
    /* find the (state, inputs) pairs with a post outside the safe set */
    BDD F = m_tr.AndAbstract(!Z,m_cube_post);
    /* the remaining (state, input) pairs make up the pre */
    BDD preZ= m_tr_nopost & (!F);
    return preZ;
  }
};
/**
@class CoopAnalysis

@brief Analyzes the cooperation region of a controller

Takes a system and a memoryless controller.
Enforces an independence relationship among different controller groups.
Provides an operator that can be called on statespace subsets Z and returns 
states where membership in Z can be enforced at the next time step.

**/

template<class state_type, class input_type>
class CoopAnalysis: public EnfPre{
private:
  std::vector<std::vector<int> > ind;
  /*Model*/
  const SymbolicModel<state_type,input_type>& model;
  const Cudd& mgr;
  /*Independent Controller BDD*/
  BDD IC;
  BDD safe; 
  BDD post_grid; 

public:
  /*
   * @brief Constructor for CoopAnalysis
   * @param [in] manager - the Cudd manager
   * @param [in] transition_relation - the BDD encoding the transition function of the SymbolicModel\n 
   *                              computed with SymbolicModel::compute_gb
   * @param [in] model - SymbolicModel containing the SymbolicSet for the state and input alphabet 
   * @param [in] indep - Independent controller groups. indep.size() is equal to total number of groups.
   * @param [in] ctrl  - Memoryless controller expressed as (state x input) relation 
  */
  CoopAnalysis(const Cudd& manager, 
         const BDD& trans_relation,
         const SymbolicModel<state_type,input_type>& sym_model,
         std::vector<std::vector<int> > indep,
         const BDD& ctrl): EnfPre(manager, trans_relation, sym_model), model(sym_model), mgr(manager){
    ind = indep;
    post_grid = sym_model.get_sym_set_post().get_grid_bdd(manager);
    /*Safe controllable region*/    
    safe = ctrl.ExistAbstract(m_cube_input);
    /*Take controller and impose independence w.r.t. ind*/
    IC = safe;
    BDD U_slice = mgr.bddOne();
    for (unsigned long j = 0; j < ind.size(); j++){
      U_slice = SymbolicSet(model.get_sym_set_input(), ind[j]).get_cube(mgr);
      // Existentially abstract away current slice from group to get everything else
      U_slice = model.get_sym_set_input().get_cube(mgr).ExistAbstract(U_slice);
      // Existentially abstract away remaining inputs not in group from controller
      IC = IC & (ctrl.ExistAbstract(U_slice));
    }
  }

  /**
  @brief Returns states that can remain in set Z without cooperation in the controller.
  @param [in] -  Z Desired end set Z
  @result  Set of states for which states at next time step are contained in Z
  **/
  BDD operator()(BDD Z) const {
    /* project onto pre state alphabet */
    Z=Z.ExistAbstract(m_cube_post*m_cube_input);
    /* swap pre and post variables */
    BDD postZ=Z.Permute(m_permute.get());
    /*Get states that are safe with coordination, unsafe without it*/
    BDD preIndep = ((m_tr & IC).ExistAbstract(m_cube_input) & (!postZ & post_grid)).ExistAbstract(m_cube_post);
    /*Return states that are safe without coordination*/
    return (Z & (!preIndep));
  }
};

/**
@class CooperativeEnfPre
@brief Operator to synthesize controller that is aware of coordination restrictions.
This operator is NOT monotone and has no convergence guarantees.
**/
template<class state_type, class input_type>
class CooperativeEnfPre: public EnfPre{
private:
  
  std::vector<std::vector<int> > ind;
  /*Model*/
  const SymbolicModel<state_type,input_type>& model;
  const Cudd& mgr;
public:

  /**
   * @brief Constructor for CooperativeEnfPre
   * @param [in] manager - the Cudd manager
   * @param [in] transition_relation - the BDD encoding the transition function of the SymbolicModel\n 
   *                              computed with SymbolicModel::compute_gb
   * @param [in] model - SymbolicModel containing the SymbolicSet for the state and input alphabet 
   * @param [in] indep - Independent controller groups. indep.size() is equal to total number of groups.
  **/
  CooperativeEnfPre(const Cudd& manager, 
         const BDD& transition_relation,
         const SymbolicModel<state_type,input_type>& sym_model,
         std::vector<std::vector<int> > indep): EnfPre(manager, transition_relation, sym_model), model(sym_model), mgr(manager){
    ind = indep;
  }

  /** @brief computes the enforcable predecessor of the BDD Z subject to
  control independence constraint **/
  BDD operator()(BDD Z) const {
    /* project onto pre state alphabet */
    Z=Z.ExistAbstract(m_cube_post*m_cube_input);
    /* swap pre and post variables */
    Z=Z.Permute(m_permute.get());
    /* find the (state, inputs) pairs with a post outside the safe set */
    std::cout << "\nComputing Exact Control" << std::endl;
    BDD F = m_tr.AndAbstract(!Z,m_cube_post);
    /* the remaining (state, input) pairs make up the enforceable pre */
    BDD preZ= m_tr_nopost & (!F);

    /* Construct minimal independent set different input group dims*/
    BDD indep_control = mgr.bddOne(), U_slice = mgr.bddOne();
    for (unsigned long j = 0; j < ind.size(); j++){
      // Get the group of inputs associated with ind[j]
      U_slice = SymbolicSet(model.get_sym_set_input(), ind[j]).get_cube(mgr);

      // Existentially abstract away current group from set to get everything else
      U_slice = model.get_sym_set_input().get_cube(mgr).ExistAbstract(U_slice);

      // Existentially abstract away remaining inputs not in group from controller
      indep_control = indep_control & (preZ.ExistAbstract(U_slice));
    }

    // All sanity checks should be true
    std::cout << "--- Cooperation Free Control Sanity Checks ---" << std::endl;
    std::cout << "Different Control: " << (int) (indep_control != preZ) << std::endl;
    std::cout << "Subset: " << (int) ((preZ & (!indep_control)) == mgr.bddZero()) << std::endl;
    std::cout << "Strict Subset: " << (int) ((!preZ & indep_control) != mgr.bddZero()) << std::endl;
    /*Get states that are safe with coordination, unsafe without it*/
    BDD preIndep = ((m_tr & indep_control).ExistAbstract(m_cube_input) & (!Z)).ExistAbstract(m_cube_post);
    std::cout << "Computed Unsafe without Cooperation States" << std::endl;
    /*Return states that are safe without coordination*/
    return (preZ.ExistAbstract(m_cube_input) & (!preIndep));
  }

};

/** @brief: small function to output progess of an iteration to the terminal **/
inline void print_progress(int i) {
  std::cout << ".";
  std::flush(std::cout);
  if(!(i%40)) {
    std::cout << "\r";
    std::cout << "                                        ";
    std::cout << "\r";
  }
}


} /* close namespace */
#endif /* ENFPRE_HH_ */
