import matplotlib.pyplot as plt

a = []
with open("out.txt") as file:
    for line in file:
        sline =line.rstrip(' \n').split(' ')
        if ('winning' in sline):
            a.append(int(sline[-1]))
            #sline[0].split('-')[0])

lines = plt.plot(a)
# plt.figure(figsize=(6, 4))
plt.setp(lines[0], linewidth=5, color='r')
plt.title("Size of Communication-free Safe Region")
plt.xlabel('Communization Horizon')
plt.ylabel("# States")
plt.savefig("out.png", dpi = 200, format = 'png')
