/*
 * dynamic_topo.cc
 *
 *  created: Jul 2017
 *   author: Eric Kim 
 */

/** @file 

Two vehicles are initially in different lanes, but need to cooperate to safely execute a lane change.

@todo Three agents system with a dynamic connection topology
**/

#include <iostream>
#include <array>

/* SCOTS header */
#include "scots.hh"
/* ode solver */
#include "RungeKutta4.hh"

/* time profiling */
#include "TicToc.hh"
/* memory profiling */
#include <sys/time.h>
#include <sys/resource.h>

#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/connected_components.hpp>

struct rusage usage;

/* state space dim */
const int state_dim=5;
/* input space dim */
const int input_dim=3;

/* sampling time */
const double tau = 0.1;

const double car_len = 4.5;

/*
 * data types for the state space elements and input space
 * elements used in uniform grid and ode solvers
 */
using state_type = std::array<double,state_dim>;
using input_type = std::array<double,input_dim>;

/* abbrev of the type for abstract states and inputs */
using abs_type = scots::abs_type;

/* we integrate the vehicle ode by tau sec (the result is stored in x)  */
/**
@brief Function that encodes system dynamics
Takes a state and input and returns the next/post state.
**/
auto vehicle_post = [](state_type &x, const input_type &u) {
  /* the ode describing the vehicle */
  auto rhs =[](state_type& xx,  const state_type &x, const input_type &u) {
    xx[0] = x[1]; // x1 pos
    xx[1] = u[0]; // v1 vel

    xx[2] = x[3]; // x2 pos
    xx[3] = u[1]; // v2 vel
    xx[4] = u[2]; // y2 pos
  };
  /* simulate (use 10 intermediate steps in the ode solver) */
  scots::runge_kutta_fixed4(rhs,x,u,state_dim,tau,15);
};

/* we integrate the growth bound by tau sec (the result is stored in r)  */
/**
@brief Function that quantifies uncertainty in next/post state due to state and input discretization

The error should have identical dimension with the state space
**/
auto radius_post = [](state_type &, const state_type &, const input_type &) {
  // Argument order r, x, u
  // r[0] = r[0] + tau * r[2];
  // r[1] = r[1] + tau * r[3];
  // r[2] = r[2] + .1 * tau;
  // r[3] = r[3] + .1 * tau;
};

int main() {
  /* to measure time */
  TicToc tt;
  /* cudd manager */
  Cudd mgr;
  mgr.AutodynEnable();
  //mgr.AutodynDisable();

  /* try to read data from files */
  scots::SymbolicSet ss_pre;
  scots::SymbolicSet ss_post;
  if(!scots::read_from_file(mgr,ss_pre,"vehicle_state_pre") ||
     !scots::read_from_file(mgr,ss_post,"vehicle_state_post")) {
   /* lower bounds of the hyper rectangle */
    state_type s_lb={{0,25,0,28,0}};
    /* upper bounds of the hyper rectangle */
    state_type s_ub={{300,30,300, 42, 8}};
    /* grid node distance diameter */
    state_type s_eta={{1,.2,1,.2,.2}};
    /* construct SymbolicSet with the UniformGrid information for the state space
     * and BDD variable IDs for the pre */
    ss_pre = scots::SymbolicSet(mgr, state_dim,s_lb,s_ub,s_eta);
    /* construct SymbolicSet with the UniformGrid information for the state space
     * and BDD variable IDs for the post */
    ss_post = scots::SymbolicSet(mgr, state_dim,s_lb,s_ub,s_eta);
 
    scots::write_to_file(ss_pre,"vehicle_state_pre");
    scots::write_to_file(ss_post,"vehicle_state_post");
  }
  std::cout << "Unfiorm grid details:" << std::endl;
  ss_pre.print_info(1);

  /* try to read data from files */
  scots::SymbolicSet ss_input;
  if(!scots::read_from_file(mgr,ss_input,"vehicle_input_alphabet")){
    /* construct grid for the input space */ 
    /* lower bounds of the hyper rectangle */
    input_type i_lb={{-0.9,-0.9,-1}};
    /* upper bounds of the hyper rectangle */
    input_type i_ub={{ .9, .9,1}};
    /* grid node distance diameter */
    input_type i_eta={{.1,.1,.2}};
    ss_input = scots::SymbolicSet(mgr, input_dim,i_lb,i_ub,i_eta); 
    scots::write_to_file(ss_input,"vehicle_input_alphabet");
  }
  ss_input.print_info(1);

  scots::SymbolicSet set;
  /* initialize SymbolicModel class with the abstract state and input alphabet */
  scots::SymbolicModel<state_type,input_type> sym_model(ss_pre,ss_input,ss_post);
  //set = scots::SymbolicSet(scots::SymbolicSet(ss_pre,ss_input),ss_post);

  /* Declare dependency and print out */
  scots::DT_Dependency dep(state_dim,input_dim);
  std::vector<std::vector<int> > state_rhs({{0,1},{1},{2,3}, {3}, {4}}), input_rhs({{},{0},{},{1}, {2}});
  dep.set_rhs(state_rhs, input_rhs);
  std::cout << dep;

  /* Sparse abstraction*/
  BDD sparse_TF;
  if(!scots::read_from_file(mgr,set,sparse_TF,"vehicle_sparse_tf")) {

    std::cout << "\nComputing the sparse transition function: " << std::endl;
    tt.tic();
    size_t no_trans;
    sparse_TF = sym_model.compute_sparse_gb(mgr,vehicle_post,radius_post,dep, no_trans);
    tt.toc();
    std::cout << "Number of transitions: " << no_trans << std::endl;
    if(!getrusage(RUSAGE_SELF, &usage))
      std::cout << "Memory per transition: " << usage.ru_maxrss/(double)no_trans << std::endl;

   scots::write_to_file(mgr,set,sparse_TF,"vehicle_sparse_tf");
  }

  /* Reachable Controller Synthesis with two different CPRE operators*/ 
  /* define collision set in a lower dimensional space*/
  scots::SymbolicSet collisionSpace = scots::SymbolicSet(ss_pre,{0,2,4});
  auto collision = [&ss_pre](const abs_type& idx) {
    state_type x;
    ss_pre.itox(idx,x);
    double w = car_len/2;
    /* function returns 1 if cell associated with x is in collision set  */
    if ((x[1]-w<=x[0] + w) && (x[0]-w<=x[1]+w) && (x[2] >= 4.0))
      return true;
    return false;
  };
  std::cout << "Computing Collision Set" << std::endl;
  BDD T = collisionSpace.ap_to_bdd(mgr,collision);
  /* write collision to file */
  write_to_file(mgr,collisionSpace,T,"collision");

  /*Declare collaboration independence*/
  std::vector<std::vector<int> > indep;
  indep.push_back({1,2}); // inputs are independent
  indep.push_back({0}); // inputs are independent

  std::ofstream file;
  file.open("reachable.txt");

/* setup enforcable predecessor */
  scots::EnfPre enf_pre(mgr,sparse_TF,sym_model);
  scots::CooperativeEnfPre<state_type, input_type> cepre(mgr,sparse_TF,sym_model, indep);

  BDD X = mgr.bddZero();
  BDD XX =mgr.bddOne();
  /* the controller */
  BDD C = mgr.bddZero();
  /* BDD cube for existential abstract inputs */
  const BDD U = ss_input.get_cube(mgr);
  const BDD forcedout = !(sparse_TF.ExistAbstract(U).ExistAbstract(ss_post.get_cube(mgr)));
  /* Cooperative PRE as long as not converged */
  size_t i;
  std::cout << "Starting Iterations " << std::endl;
  tt.tic();
  for(i=1; XX != X && i <= 80; i++) {
    X = XX;
    C = enf_pre(X); // (state, input) controlled pre pairs
    XX = C.ExistAbstract(U); // states that are forced out of domain not included
    XX = (XX & (!T)) | forcedout; // no collision T or forced out
    std::cout << i << "-th winning domain size: " << ss_pre.get_size(mgr,XX) << std::endl;

    /*Print out unsafe states*/
    BDD N = ss_pre.get_grid_bdd(mgr) & X & !XX; // newly unsafe states
    std::cout << i << "-th iter unsafe states added: " <<ss_pre.get_size(mgr,N) << std::endl;
    auto a = ss_pre.bdd_to_grid_points(mgr, N, 2);
    // for(size_t j = 0; j < a.size(); j++){
    //   if (j % state_dim == 0){
    //     file << i << " ";
    //   }
    //   file << a[j] << " ";
    //   if (j % state_dim == state_dim-1)
    //     file << "\n";
    // }
  }
  tt.toc();
  std::cout << "Number of cooperative iterations: " << i << std::endl;
  file.close(); 

  std::cout << "\nChecking for Cooperation Regions\n";
  /*Start uncooperative pre iterations from the controllable pre iterations*/

  bool do_uncoop = true;
  if (do_uncoop){
    file.open("coop_reachable.txt");
    BDD safe = C.ExistAbstract(U);
    scots::CoopAnalysis<state_type, input_type> coop_prop(mgr,sparse_TF,sym_model,indep,C);
    X = mgr.bddZero();
    XX = safe;
    BDD N;
    for(i=1; XX != X; i++){
      X = XX;
      XX = coop_prop(X).ExistAbstract(U);
      XX = (XX & X & safe) | forcedout;
      std::cout << i << "-th coop winning domain size: " << ss_pre.get_size(mgr,XX) << std::endl;
      N = ss_pre.get_grid_bdd(mgr) & X & !XX; // newly unsafe states
      std::cout << i << "-th coop iter unsafe states added: " << ss_pre.get_size(mgr,N) << std::endl;
      // auto a = ss_pre.bdd_to_grid_points(mgr, N, 2);
      // for(size_t j = 0; j < a.size(); j++){
      //   if (j % state_dim == 0){
      //     file << i << " ";
      //   }
      //   file << a[j] << " ";
      //   if (j % state_dim == state_dim-1)
      //     file << "\n";
      // }
    }
    
    file.close();
  }

}
