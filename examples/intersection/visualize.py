import sys

import numpy as np
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import matplotlib.colors as mcolors

filename = "coop_reachable.txt"
pngname = "coop_reach"
dim = 4
display_from_iter = 1 # only plot iterations after this
ts = .2 # sample time

# Find num of points and other size data
pre_sizes = {} 
max_pre_iter = 0
with open(filename) as f:
    # numel = sum(1 for _ in f)
    l = 0 
    for line in f:
        x = [float(i) for i in line.rstrip(' \n').split(' ')]
        cur_pre_iter = int(np.rint(x[0]))
        # Changed pre iteration
        if max_pre_iter < cur_pre_iter:
            max_pre_iter = cur_pre_iter
            l = 0 
            pre_sizes[cur_pre_iter] = 0

        pre_sizes[cur_pre_iter] += 1 
        l += 1

safe = {}
for i in pre_sizes:
    safe[i] = np.empty([pre_sizes[i],dim])

prev_pre_iter = 0
with open(filename) as f:
    l = 0
    for line in f:
        x = [float(i) for i in line.rstrip(' \n').split(' ')]
        cur_pre_iter = int(np.rint(x[0]))
        if cur_pre_iter >= display_from_iter:
            if prev_pre_iter < cur_pre_iter:
                l = 0
                prev_pre_iter = cur_pre_iter

            safe[cur_pre_iter][l,0:dim] = x[1:(dim+1)]
            l += 1

### Impose velocity filter
for i in safe:
    mask = np.isclose(safe[i][:, 3], 2.8)
    safe[i] = safe[i][mask,:]
    # mask = np.isclose(safe[i][:, 2], -.6)
    # safe[i] = safe[i][mask,:]

### Visualization parameters
stride = 1
vcam_rate = .9
hcam_rate = 1.1

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
colors = cm.rainbow(np.linspace(0, 1, max_pre_iter - display_from_iter + 1))
last_i = 0

normalize = mcolors.Normalize(vmin= ts*max(safe.keys()), vmax= ts*min(safe.keys()))
colormap = cm.jet_r

scalarmappaple = cm.ScalarMappable(norm=normalize, cmap=colormap)
scalarmappaple.set_array([i*ts for i in safe.keys()[::-1] ])
plt.colorbar(scalarmappaple)

for i,co in zip(safe.keys(), colors[::-1]):
    # for i,co in zip([1], colors[::-1]):
    transp = float(i-display_from_iter + 1)/(max_pre_iter-display_from_iter+1)
    num_pts = safe[i].shape[0]
    if num_pts == 0:
        continue
    samples = np.random.choice(num_pts, num_pts, replace = False)
    ax.scatter(safe[i][samples[::stride],0],
               safe[i][samples[::stride],1], 
               safe[i][samples[::stride],2], c = co, edgecolors = 'face',
               alpha = .06 - .05*transp)
    ax.view_init(75 - i*vcam_rate,-51 - i * hcam_rate)
    #ax.set_title("Predecesor " + str(i))
    ax.set_xlim([-10, 10])
    ax.set_ylim([-10, 10])
    ax.set_xlabel("P1")
    ax.set_ylabel("P2")
    ax.set_zlabel("V1")
    plt.savefig(pngname + "_"+ str(i) + ".png", dpi = 200, format = 'png')
    last_i = i
    #plt.cla()

# Keep on rotating
for j in range(last_i+1,last_i+20):
    ax.view_init(75 - last_i*vcam_rate,-51 - j * hcam_rate)
    plt.savefig(pngname + "_"+ str(j) + ".png", dpi = 200, format = 'png')


# if (1):
#     fig = plt.figure()
#     ax = fig.add_subplot(111, projection='3d')
#     with open("coop_reachable.txt") as f:
#         numel = sum(1 for _ in f)
#         uncoord = np.empty([numel,dim])
#     with open("coop_reachable.txt") as f:
#         j=0
#         for line in f:
#             x = [float(i) for i in line.rstrip(' \n').split(' ')]
#             uncoord[j] = x[1:(dim+1)]
#             j += 1
#         mask = np.isclose(uncoord[:, 3], 2.8)
#         uncoord = uncoord[mask,:]
#         ax.scatter(uncoord[:,0],
#                uncoord[:,1], 
#                uncoord[:,2], c = co, edgecolors = 'face',
#                alpha = .06 - .05*transp)
#         plt.savefig("safe_uncooperative"+".png", dpi = 200, format = 'png')
#         plt.show()

