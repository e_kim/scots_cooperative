import sys

import numpy as np
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import matplotlib.colors as mcolors
import matplotlib.patches as patches

filename = "coop_reachable.txt"
pngname = "coop_reach"
dim = 2
display_from_iter = 1 # only plot iterations after this
ts = .01 # sample time

# Find num of points and other size data
pre_sizes = {} 
max_pre_iter = 0
with open(filename) as f:
    # numel = sum(1 for _ in f)
    l = 0 
    for line in f:
        x = [float(i) for i in line.rstrip(' \n').split(' ')]
        cur_pre_iter = int(np.rint(x[0]))
        # Changed pre iteration
        if max_pre_iter < cur_pre_iter:
            max_pre_iter = cur_pre_iter
            l = 0 
            pre_sizes[cur_pre_iter] = 0

        pre_sizes[cur_pre_iter] += 1 
        l += 1

safe = {}
for i in pre_sizes:
    safe[i] = np.empty([pre_sizes[i],dim])

prev_pre_iter = 0
with open(filename) as f:
    l = 0
    for line in f:
        x = [float(i) for i in line.rstrip(' \n').split(' ')]
        cur_pre_iter = int(np.rint(x[0]))
        if cur_pre_iter >= display_from_iter:
            if prev_pre_iter < cur_pre_iter:
                l = 0
                prev_pre_iter = cur_pre_iter

            safe[cur_pre_iter][l,0:dim] = x[1:(dim+1)]
            l += 1

### Visualization parameters
stride = 1

fig = plt.figure()
ax = fig.add_subplot(111)
colors = cm.rainbow(np.linspace(0, 1, max_pre_iter - display_from_iter + 1))
last_i = 0

normalize = mcolors.Normalize(vmin= ts*max(safe.keys()), vmax= ts*min(safe.keys()))
colormap = cm.jet_r

scalarmappaple = cm.ScalarMappable(norm=normalize, cmap=colormap)
scalarmappaple.set_array([i*ts for i in safe.keys()[::-1] ])
plt.colorbar(scalarmappaple)

#circle1 = plt.Circle((0, 0), 0.8, color='r')
#ax.add_artist(circle1)
ax.add_patch(
    patches.Circle(
        (0.0, 0.0),   # (x,y)
        0.8,          # radius
        fill = False
    )
)

for i,co in zip(safe.keys(), colors[::-1]):
    transp = .2# float(i-display_from_iter + 1)/(max_pre_iter-display_from_iter+1)
    num_pts = safe[i].shape[0]
    if num_pts == 0:
        continue
    samples = np.random.choice(num_pts, num_pts, replace = False)
    ax.scatter(safe[i][samples[::stride],0],
               safe[i][samples[::stride],1], 
               c = co, edgecolors = 'face',
               alpha = .26 - .25*transp)
    #ax.view_init(75 - i*vcam_rate,-51 - i * hcam_rate)
    ax.set_title("Coordination-Free Collision Region Propagation " + str(i*ts) + " time steps")
    ax.set_xlim([-1, 1])
    ax.set_ylim([-1, 1])
    ax.set_xlabel(r"$x_1$")
    ax.set_ylabel(r"$x_2$")
    plt.savefig(pngname + "_"+ str(i) + ".png", dpi = 200, format = 'png')
    last_i = i
    #plt.cla()
