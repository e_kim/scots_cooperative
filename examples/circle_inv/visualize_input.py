import sys

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import matplotlib.colors as mcolors
import matplotlib.patches as patches

filename = "allowed_controls.txt"
pngname = "allowed"
dim = 2
display_from_iter = 1 # only plot iterations after this
ts = .01 # sample time

# Find num of points and other size data
pre_sizes = {} 
max_pre_iter = 0
with open(filename) as f:
    numel = sum(1 for _ in f)


inputs = np.empty([numel,dim])

prev_pre_iter = 0
with open(filename) as f:
    l = 0
    for line in f:
        x = [float(i) for i in line.rstrip(' \n').split(' ')]
        inputs[l,0:dim] = x[0:dim]
        l += 1

### Visualization parameters
stride = 1

fig = plt.figure()
ax = fig.add_subplot(111)
colors = cm.rainbow(np.linspace(0, 1, max_pre_iter - display_from_iter + 1))
last_i = 0

ax.scatter(inputs[:,0], inputs[:,1], alpha = .3)
ax.set_xlim([-1, 1])
ax.set_ylim([-1, 1])
ax.set_xlabel(r"$u_1$")
ax.set_ylabel(r"$u_2$")
plt.show()


# normalize = mcolors.Normalize(vmin= ts*max(inputs.keys()), vmax= ts*min(inputs.keys()))
# colormap = cm.jet_r

# scalarmappaple = cm.ScalarMappable(norm=normalize, cmap=colormap)
# scalarmappaple.set_array([i*ts for i in inputs.keys()[::-1] ])
# plt.colorbar(scalarmappaple)

# for i,co in zip(inputs.keys(), colors[::-1]):
#     transp = float(i-display_from_iter + 1)/(max_pre_iter-display_from_iter+1)
#     num_pts = inputs[i].shape[0]
#     if num_pts == 0:
#         continue
#     samples = np.random.choice(num_pts, num_pts, replace = False)
#     ax.scatter(inputs[i][samples[::stride],0],
#                inputs[i][samples[::stride],1], 
#                c = co, edgecolors = 'face',
#                alpha = .26 - .25*transp)
#     #ax.view_init(75 - i*vcam_rate,-51 - i * hcam_rate)
#     ax.set_title("Coordination-Free Collision Region Propagation " + str(i*ts) + " time steps")
#     ax.set_xlim([-1, 1])
#     ax.set_ylim([-1, 1])
#     ax.set_xlabel(r"$x_1$")
#     ax.set_ylabel(r"$x_2$")
#     plt.savefig(pngname + "_"+ str(i) + ".png", dpi = 200, format = 'png')
#     last_i = i
#     #plt.cla()
