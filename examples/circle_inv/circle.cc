/*
 * circle.cc
 *
 *  created: Jul 2017
 *   author: Eric Kim 
 */

/** @file 

Circle example where two agents have independent dynamics but must remain in a coupled area.
**/

/*
 * information about this example is given in
 * 
 */

#include <iostream>
#include <array>

/* SCOTS header */
#include "scots.hh"
/* ode solver */
#include "RungeKutta4.hh"

/* time profiling */
#include "TicToc.hh"
/* memory profiling */
#include <sys/time.h>
#include <sys/resource.h>

#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/connected_components.hpp>

struct rusage usage;

/* state space dim */
const int state_dim=2;
/* input space dim */
const int input_dim=2;

/* sampling time */
const double tau = 0.01;

/*
 * data types for the state space elements and input space
 * elements used in uniform grid and ode solvers
 */
using state_type = std::array<double,state_dim>;
using input_type = std::array<double,input_dim>;

/* abbrev of the type for abstract states and inputs */
using abs_type = scots::abs_type;

/* we integrate the circle ode by tau sec (the result is stored in x)  */
/**
@brief Function that encodes system dynamics
Takes a state and input and returns the next/post state.
**/
auto circle_post = [](state_type &x, const input_type &u) {
  /* the ode describing the circle */
  auto rhs =[](state_type& xx,  const state_type &x, const input_type &u) { 
    xx[0] = u[0]; // velocity
    xx[1] = u[1]; // velocity
  };
  /* simulate (use 10 intermediate steps in the ode solver) */
  scots::runge_kutta_fixed4(rhs,x,u,state_dim,tau,15);
};


/* we integrate the growth bound by tau sec (the result is stored in r)  */
/**
@brief Function that quantifies uncertainty in next/post state due to state and input discretization

The error should have identical dimension with the state space
**/
auto radius_post = [](state_type &r, const state_type &, const input_type &) {
  r[0] = r[0] + .005 * tau;
  r[1] = r[1] + .005 * tau;
};

/**
@brief Executes controller synthesis and analysis procedure
**/
int main() {
  /* to measure time */
  TicToc tt;
  /* cudd manager */
  Cudd mgr;
  mgr.AutodynEnable();
  //mgr.AutodynDisable();

  /* try to read data from files */
  scots::SymbolicSet ss_pre;
  scots::SymbolicSet ss_post;
  if(!scots::read_from_file(mgr,ss_pre,"circle_state_pre") ||
     !scots::read_from_file(mgr,ss_post,"circle_state_post")) {
   /* lower bounds of the hyper rectangle */
    state_type s_lb={{-1,-1}};
    /* upper bounds of the hyper rectangle */
    state_type s_ub={{1,1}};
    /* grid node distance diameter */
    state_type s_eta={{.01,.01}}; 
    /** construct SymbolicSet with the UniformGrid information for the state space
     * and BDD variable IDs for the pre */
    ss_pre = scots::SymbolicSet(mgr, state_dim,s_lb,s_ub,s_eta);
    /** construct SymbolicSet with the UniformGrid information for the state space
     * and BDD variable IDs for the post */
    ss_post = scots::SymbolicSet(mgr, state_dim,s_lb,s_ub,s_eta);
 
    scots::write_to_file(ss_pre,"circle_state_pre");
    scots::write_to_file(ss_post,"circle_state_post");
  }
  std::cout << "Unfiorm grid details:" << std::endl;
  ss_pre.print_info(1);

  /* try to read data from files */
  scots::SymbolicSet ss_input; /** Input space **/
  if(!scots::read_from_file(mgr,ss_input,"circle_input_alphabet")){
    /* construct grid for the input space */ 
    /* lower bounds of the hyper rectangle */
    input_type i_lb={{-1.0,-1.0}};
    /* upper bounds of the hyper rectangle */
    input_type i_ub={{ 1.0, 1.0}};
    /* grid node distance diameter */
    input_type i_eta={{.05,.05}};
    ss_input = scots::SymbolicSet(mgr, input_dim,i_lb,i_ub,i_eta); 
    scots::write_to_file(ss_input,"circle_input_alphabet");
  } 
  ss_input.print_info(1);

  scots::SymbolicSet set;
  /* initialize SymbolicModel class with the abstract state and input alphabet */
  scots::SymbolicModel<state_type,input_type> sym_model(ss_pre,ss_input,ss_post);
  set = scots::SymbolicSet(scots::SymbolicSet(ss_pre,ss_input),ss_post);

  /* Declare dependency and print out */
  scots::DT_Dependency dep(state_dim,input_dim);
  std::vector<std::vector<int> > state_rhs({{0},{1}}), input_rhs({{0},{1}});
  dep.set_rhs(state_rhs, input_rhs);
  std::cout << dep;

  /* Sparse abstraction*/
  BDD sparse_TF;
  if(!scots::read_from_file(mgr,set,sparse_TF,"circle_sparse_tf")) {

    std::cout << "\nComputing the sparse transition function: " << std::endl;
    tt.tic();
    size_t no_trans;
    sparse_TF = sym_model.compute_sparse_gb(mgr,circle_post,radius_post,dep, no_trans);
    tt.toc();
    std::cout << "Number of transitions: " << no_trans << std::endl;
    if(!getrusage(RUSAGE_SELF, &usage))
      std::cout << "Memory per transition: " << usage.ru_maxrss/(double)no_trans << std::endl;

   scots::write_to_file(mgr,set,sparse_TF,"circle_sparse_tf");
  }

  /* Reachable Controller Synthesis with two different CPRE operators*/ 
  /* define collision set */ 
  auto collision = [&ss_pre](const abs_type& idx) {
    state_type x;
    ss_pre.itox(idx,x);
    /* function returns 1 if cell associated with x is in collision set  */
    if (x[0]*x[0] + x[1] * x[1] > .63999)
      return true;
    return false;
  };
  BDD T = ss_pre.ap_to_bdd(mgr,collision);
  /* write collision to file */
  write_to_file(mgr,ss_pre,T,"collision");

  /*Declare collaboration independence for 
    coordination region analysis  */
  std::vector<std::vector<int> > indep;
  indep.push_back({1}); // inputs are chosen independently
  indep.push_back({0}); // inputs are chosen independently

  std::ofstream file;
  file.open("reachable.txt");

  /** setup enforcable predecessor **/
  scots::EnfPre enf_pre(mgr,sparse_TF,sym_model);
  scots::CooperativeEnfPre<state_type, input_type> cepre(mgr,sparse_TF,sym_model, indep);
  BDD X = mgr.bddZero();
  BDD XX =mgr.bddOne();

  BDD C = mgr.bddZero();   /* the controller */
  const BDD U = ss_input.get_cube(mgr); /* BDD cube for existential abstract inputs */
  /* Set of blocking states*/
  const BDD forcedout = !(sparse_TF.ExistAbstract(U).ExistAbstract(ss_post.get_cube(mgr)));

  /** 
  * Synthesize controllers with cooperative PRE as long as not 
  * converged or reached maximum fixed point
  **/
  size_t i;
  std::cout << "Starting Iterations " << std::endl;
  tt.tic();
  for(i=1; XX != X && i <= 100; i++) {
    X = XX;
    C = enf_pre(X); // (state, input) controlled pre pairs
    XX = C.ExistAbstract(U); // states that are forced out of domain not included
    XX = (XX & (!T)); // no collision T or forced out
    std::cout << i << "-th winning domain size: " << ss_pre.get_size(mgr,XX) << std::endl;

    /*Print out unsafe states*/
    BDD N = ss_pre.get_grid_bdd(mgr) & X & !XX; // newly unsafe states
    std::cout << i << "-th iter unsafe states added: " <<ss_pre.get_size(mgr,N) << std::endl;
    auto a = ss_pre.bdd_to_grid_points(mgr, N, 1);
    for(size_t j = 0; j < a.size(); j++){
      if (j % state_dim == 0){
        file << i << " ";
      }
      file << a[j] << " ";
      if (j % state_dim == state_dim-1)
        file << "\n";
    }
  }
  tt.toc();
  std::cout << "Number of cooperative iterations: " << i << std::endl;
  file.close(); 

  /* Print out admissible control actions at a single state */
  file.open("allowed_controls.txt");
  int x_id = 10090; 
  BDD u = (ss_pre.id_to_bdd(x_id) & C);
  auto point = ss_pre.bdd_to_grid_points(mgr, ss_pre.id_to_bdd(x_id));
  for (size_t j = 0; j < point.size();  j++)
    std::cout << point[j] << "  ";
  std::cout << "\n";
  auto a = ss_input.bdd_to_grid_points(mgr, u, 1);
  for(size_t j = 0; j < a.size(); j++){
      file << a[j] << " ";
      if (j % input_dim == input_dim-1)
        file << "\n";
  }
  file.close();

  /**
  Identify safe (with timing information) region without cooperation
  Start uncooperative pre iterations from the controllable pre iterations
  **/
  bool do_uncoop = true;
  if (do_uncoop){
    std::cout << "\nChecking for Cooperation Regions\n";
    file.open("coop_reachable.txt");
    BDD safe = C.ExistAbstract(U);
    scots::CoopAnalysis<state_type, input_type> coop_prop(mgr,sparse_TF,sym_model,indep,C);
    X = mgr.bddZero();
    XX = safe;
    BDD N;
    for(i=1; XX != X; i++){
      X = XX;
      XX = coop_prop(X).ExistAbstract(U);
      XX = (XX & X & safe);
      std::cout << i << "-th coop winning domain size: " << ss_pre.get_size(mgr,XX) << std::endl;
      N = ss_pre.get_grid_bdd(mgr) & X & !XX & !T; // newly unsafe states
      std::cout << i << "-th coop iter unsafe states added: " << ss_pre.get_size(mgr,N) << std::endl;
      auto a = ss_pre.bdd_to_grid_points(mgr, N, 1);
      for(size_t j = 0; j < a.size(); j++){
        if (j % state_dim == 0){
          file << i << " ";
        }
        file << a[j] << " ";
        if (j % state_dim == state_dim-1)
          file << "\n";
      }
    }
    
    file.close();
  }

}
